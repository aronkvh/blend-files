| Blend File                          | Start (Frames - Seconds) | End (Frames - Seconds) |
| ----------------------------------- | ------------------------ | ---------------------- |
| Intro                               | 0 - 0                    | 482 - 16+02            |
| 3D Room                             | 482 -16+02               | 815 - 27+05            |
| Old Version Showcase                | 815 - 27+05              | 1632 - 52+12           |
| Main Content Start                  | 1632 - 52+12             | 1808 - 01:00+08        |
| Animated Desktop and Power Profiles | 1785 - 59+15             | 2406 - 1:20+06         |
| Color Accent Title                  | 2406 - 1:20+06           | 2491 - 1:23+01         |
| Accent Colors 3D                    | 2491 - 1:23+01           | 2608 -1:26+28          |
| Accent Colors 2D                    | 2608 -1:26+28            | 2719 -1:30+19          |
